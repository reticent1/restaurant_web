import React from 'react';
import path from './food1.jpg';
import './home.css';
import WOW from 'wowjs'
import '../css/animate.css'
class Home extends React.Component {
    componentDidMount(){
        const wow = new WOW.WOW();
        wow.init()
    }

    render() {
        return (
            <div className="container">
                <img alt="900x500" src={path} className="img-home"/>
                <div className="centered">
                    <p className="slogan wow fadeInDownBig">Begin your TASTE journey with us...</p>
                </div>
            </div>
        );
    }
}

export default Home;