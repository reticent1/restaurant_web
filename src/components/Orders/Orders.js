import React from 'react'
import PropTypes from 'prop-types'
import './Orders.css'
import Popup from 'reactjs-popup';
import _ from 'lodash';

class Orders extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            onMobile: '',
            number: 0,
            hide: false,
            verification: 0,
            userInfo: {},
            mobile: [],
            address: [],
            name: '',
            show: true,
            hidder: false,
            useNumber: '',
            addrHide: true,
            addhidder: false,
            useAddress: '',
            useAddressType: '',
            useAddressLandmark: '',
            isScroll: false,
            scrollBtn: false,
            mobBlank: true,
            mobValid: true,
            btnDisable: true,
            mobAlready: true,
            verifyCode: true,
            selectNo: true,
            selectAdd: true,
            enterLand: true,
            enterAdd: true,
            selectAddress:true
        }
    }

    renderOrder = (key) => {
        const menu = this.props.menus[key];
        const count = this.props.order[key];
        const removeButton = <button className="removeBtn" onClick={() => this.props.removeOrder(key)}>&times;</button>

        return (
            <li key={key}>
                <span className="order-name">
					<span key={count}>{ count } </span>
                    {menu.item_name} { removeButton }
				</span>
                <span className="price">{count * menu.price} Rs</span>
            </li>
        )
    }

    checkOut = (orderIds, total, bool) => {
        let orders = []
        let counts = []
        let userOrder = []
        orderIds.map(key => {
            orders[key] = this.props.menus[key]
            counts[key] = this.props.order[key]
        })
        console.log(orders)
        console.log(counts)
        console.log(total)
        console.log(this.state.useNumber)
        console.log(this.state.useAddress)
        console.log(this.state.useAddressType)
        console.log(this.state.useAddressLandmark)

        orders.map((data, key) => {
            userOrder[key] = {
                item_name: data.item_name,
                quantity: counts[key],
                price: (data.price) * (counts[key])
            }
        })

        userOrder = _.compact(userOrder)
        console.log(userOrder)

        const {useAddressLandmark, useAddress}=this.state;
        if (useAddressLandmark.trim() !== "" && useAddress.trim() !== "") {
            if (bool === true) {
                fetch('http://localhost:3000/profile/address/add', {
                    method: 'post',
                    headers: {'content-type': 'application/json'},
                    body: JSON.stringify({
                        user_id: this.state.userInfo[0].id,
                        type: this.state.useAddressType,
                        landmark: this.state.useAddressLandmark,
                        complete_address: this.state.useAddress
                    })
                })
                    .then(response => response.json())
                    .then(console.log)
                    .catch(console.log)
            }

            fetch('http://localhost:3000/menu/order', {
                method: 'post',
                headers: {'content-type': 'application/json'},
                body: JSON.stringify({
                    user_id: this.state.userInfo[0].id,
                    order: userOrder,
                    mob_no: this.state.useNumber,
                    type_of_add: this.state.useAddressType,
                    user_add: this.state.useAddress,
                    landmark: this.state.useAddressLandmark,
                    total: total
                })
            })
                .then(response => response.json())
                .then(console.log)
                .catch(console.log)

            this.props.onRouteChange('payment')
        } else if (useAddressLandmark.trim() === "") {
            this.setState({enterLand: false})
        } else if (useAddress.trim() === "") {
            this.setState({enterAdd: false})
        }
    }

    onMobileChange = (event) => {
        let val = Math.floor(1000 + Math.random() * 9000);
        const mobValidator = /^\d{10}$/;
        if (event.target.value.match(mobValidator)) {
            this.setState({
                onMobile: event.target.value,
                number: val,
                useNumber: event.target.value,
                mobValid: true,
                btnDisable: false
            })
            console.log(val)
        } else if (event.target.value.trim() === "") {
            this.setState({mobBlank: false, mobValid: true, mobAlready: true})
        } else {
            this.setState({mobValid: false, mobBlank: true, mobAlready: true})
        }
    }

    onLandMark = (event) => {
        if (event.target.value.trim() === '') {
            this.setState({enterLand: false})
        } else {
            this.setState({useAddressLandmark: event.target.value})
            this.setState({enterLand: true})
        }
    }

    onCompleteAddress = (event) => {
        if (event.target.value.trim() === "") {
            this.setState({enterAdd: false})
        } else {
            this.setState({enterAdd: true})
            this.setState({useAddress: event.target.value})
        }
    }

    onType = (event) => {
        if (event.target.id === '1') {
            this.setState({useAddressType: 'Home', selectAdd: false})
        }
        if (event.target.id === '2') {
            this.setState({useAddressType: 'Work', selectAdd: false})
        }
    }

    messageSend = () => {
        if (this.state.onMobile.trim() === "") {
            this.setState({mobBlank: false, mobValid: true})
        } else {
            let found = false
            {
                this.state.mobile.map((data, index) => {
                    if (data.mobile.toString() === this.state.onMobile.toString()) {
                        found = true
                        this.setState({mobAlready: false})
                    }
                })
            }
            if (!found) {
                this.setState({hide: true})
                fetch('http://localhost:3000/send', {
                    method: 'post',
                    headers: {'content-type': 'application/json'},
                    body: JSON.stringify({
                        toNumber: '+91' + this.state.onMobile,
                        message: this.state.number + " is your verification code"
                    })
                })
                    .then(response => response.json())
            }
        }
    }

    onVerification = (event) => {
        this.setState({verification: event.target.value})
    }

    messageVerify = () => {
        if (this.state.verification.toString() === this.state.number.toString()) {
            console.log('Mobile number verified Successfully')
            console.log(this.state.useNumber)
            this.setState({hidder: true})
            fetch('http://localhost:3000/profile/contact/add', {
                method: 'post',
                headers: {'content-type': 'application/json'},
                body: JSON.stringify({
                    user_id: this.state.userInfo[0].id.toString(),
                    mobile: this.state.useNumber
                })
            })
                .then(response => response.json())
                .then(data => {
                    if (data === 'not entered') {
                        this.setState({mobAlready: false, hide: false, hidder: false})
                    }
                    if (this.state.address.length === 0 && data !== 'not entered') {
                        this.setState({addrHide: false})
                    }
                })
                .catch(error => console.log(error))


        } else {
            this.setState({verifyCode: false})
        }
    }

    componentWillMount() {
        fetch('http://localhost:3000/profile/user', {
            method: 'post',
            headers: {'content-type': 'application/json'},
            body: JSON.stringify({
                email: this.props.email
            })
        })
            .then(response => response.json())
            .then(user => {
                this.setState({userInfo: user})
                this.props.name(user[0].name)
                console.log(user[0].name);

                fetch('http://localhost:3000/profile/contact', {
                    method: 'post',
                    headers: {'content-type': 'application/json'},
                    body: JSON.stringify({
                        id: this.state.userInfo[0].id
                    })
                })
                    .then(response => response.json())
                    .then(contacts => {
                        if (contacts === 'not found' || contacts.length === 0) {
                            this.setState({show: false})
                        } else {
                            this.setState({mobile: contacts})
                            console.log(this.state.mobile)
                        }
                    })
                    .catch(error => {
                        console.log(error)
                    })

                fetch('http://localhost:3000/profile/address', {
                    method: 'post',
                    headers: {'content-type': 'application/json'},
                    body: JSON.stringify({
                        id: this.state.userInfo[0].id
                    })
                })
                    .then(response => response.json())
                    .then(addresses => {
                        if (addresses === 'not found') {
                            console.log('Record not found')
                        } else {
                            this.setState({address: addresses})
                            console.log(this.state.address)
                        }
                    })

                if (this.state.mobile.mobile === '') {
                    this.setState({show: false})
                }
            })


    }

    componentDidMount() {
        window.addEventListener('scroll', this.handleScroll);
    }

    handleScroll = (event) => {
        if (window.scrollY > 0) {
            this.setState({scrollBtn: true})
        }
        if (window.scrollY > 250) {
            this.setState({isScroll: true})
        }
        if (window.scrollY === 0) {
            this.setState({scrollBtn: false})
        }
        if (window.scrollY === 250 || window.scrollY < 250) {
            this.setState({isScroll: false})
        }
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleScroll)
    }

    addNumber = () => {
        this.setState({show: false})
    }

    oldNumber = (key) => {
        this.setState({useNumber: this.state.mobile[key].mobile, selectNo: false})
    }

    passNumber = () => {
        console.log(this.state.useNumber)
        this.setState({hidder: true})
        if (this.state.address.length === 0) {
            this.setState({addrHide: false})
        }
    }

    addAddress = () => {
        this.setState({addrHide: false})
    }

    oldAddress = (key) => {
        this.setState({
            useAddress: this.state.address[key].complete_address,
            useAddressType: this.state.address[key].type,
            useAddressLandmark: this.state.address[key].landmark,
            selectAddress:false
        })
    }

    render() {
        const orderIds = Object.keys(this.props.order);
        const total = orderIds.reduce((prevTotal, key) => {
            const menus = this.props.menus[key];
            const count = this.props.order[key];
            return prevTotal + (count * menus.price || 0)
        }, 0);

        return (
            <div className="hover-menu"><h3>View Menu</h3>
                <div className={`underline ${this.state.isScroll ? 'scrolled-header' : ''}`}
                     hidden={total === 0 ? true : false}><h2>Your Order</h2></div>
                <div className={`order-container ${this.state.isScroll ? 'scrolled' : ''}`}
                     hidden={total === 0 ? true : false}>
                    {orderIds.map(this.renderOrder)}
                </div>
                <div className={`total ${this.state.scrollBtn ? 'scrolled-total' : ''}`}
                     hidden={total === 0 ? true : false}>SubTotal : {total} Rs.
                </div>
                <div className={`popup-btn ${this.state.scrollBtn ? 'scrolled-btn' : ''}`}
                     hidden={total === 0 ? true : false}>
                    <Popup trigger={<button className="popup-btn-design" disabled={total === 0 ? true : false}>
                        Proceed
                    </button>} modal contentStyle={{'width': 'auto','borderRadius':'5px'}}>
                        <div hidden={this.state.show}>
                            <div hidden={this.state.hide}>
                                <h1 className="mob-header">Please enter your mobile number</h1>
                                <hr/>
                                <div>
                                    <label style={{'font': '14px baskerville', 'paddingRight': '5px'}}>+91</label>
                                    <input type="text" name="mobile" placeholder="9876543210"
                                           onChange={this.onMobileChange} className="popup-text"/>
                                    <button onClick={this.messageSend} className="popup-cont-btn"
                                            disabled={this.state.btnDisable}>Enter
                                    </button>
                                </div>
                                <span hidden={this.state.mobBlank} className="error-span">Please enter mobile no.</span>
                                <span hidden={this.state.mobValid} className="error-span">Please enter valid 10 digit mobile no.</span>
                                <span hidden={this.state.mobAlready} className="error-span">Mobile number already registered.</span>
                            </div>
                            <div hidden={!this.state.hide || this.state.hidder}>
                                <h1 className="mob-header">Verfiy your mobile number</h1>
                                <hr/>
                                <div className="">
                                    <p style={{'font': '16px baskerville', 'margin': '5px'}}>Verification code send
                                        on {this.state.onMobile}</p>
                                </div>
                                <div>
                                    <input className="popup-text" type="text" name="verify" placeholder="7896"
                                           onChange={this.onVerification}
                                           style={{'width': '50%', 'height': '16px'}}/><br/>
                                    <button className="popup-cont-btn" onClick={this.messageVerify}
                                            style={{'float': 'none', 'margin': '10px'}}>Verify
                                    </button>
                                </div>
                                <span hidden={this.state.verifyCode} className="error-span">Verification code not matched.</span>
                            </div>
                        </div>
                        <div hidden={!this.state.show || this.state.hidder}>
                            <h1 className="mob-header">Choose or add your mobile number</h1>
                            <hr/>
                            <div>
                                {this.state.mobile.map((data, key) => {
                                    return (
                                        <div className="popup-choose-mob">
                                            <input type="radio" value={data.mobile} name="mobile" key={key} id={key}
                                                   onClick={()=>this.oldNumber(key)}
                                                   className="radio-btn"/>{data.mobile}
                                        </div>
                                    )
                                })}
                                <hr/>
                                <button onClick={this.addNumber} className="popup-add-btn">Add New Number</button>
                                <button onClick={this.passNumber}
                                        className={`popup-cont-btn ${this.state.selectNo ? 'no-select' : null}`}
                                        disabled={this.state.selectNo}>Continue
                                </button>
                            </div>
                        </div>
                        <div hidden={!this.state.hidder || !this.state.addrHide }>
                            <h1 className="mob-header">Address</h1>
                            <hr/>
                            {this.state.address.map((data, key) => {
                                return (
                                    <div className="popup-choose-mob">
                                        <input type="radio" name="address" id={key} key={key}
                                               onClick={()=>this.oldAddress(key)} className="radio-btn"/>
                                        <label style={{'fontWeight': 'bold'}}>{data.type}</label><br/>
                                        <label
                                            className="popup-label">{data.complete_address + ' '} {data.landmark} </label>
                                    </div>
                                )
                            })}
                            <hr/>
                            <button onClick={this.addAddress} className="popup-add-btn">Add new Address</button>
                            <button
                                onClick={() =>this.checkOut(orderIds, total, false)}
                                disabled={this.state.selectAddress}
                                className={`popup-cont-btn ${this.state.selectAddress ? 'no-select' : null}`}>
                                Continue
                            </button>
                        </div>
                        <div hidden={this.state.addrHide}>
                            <div className="mob-header">Add new Address</div>
                            <hr/>
                            <div>
                                <label className="address-label">LandMark</label>
                                <input type="text" name="landmark" onChange={this.onLandMark}
                                       className="popup-add-text" placeholder="eg. Karve Road"/>
                                <span hidden={this.state.enterLand} className="error-span">Please enter landmark.</span>
                                <label className="address-label">Complete Address</label>
                                <input type="text" name="address" onChange={this.onCompleteAddress}
                                       className="popup-add-text" placeholder="Street name, Area, house no."/>
                                <span hidden={this.state.enterAdd}
                                      className="error-span">Please enter complete Address.</span>
                                <div>
                                    <input className="radio-btn" type="radio" id="1" name="type" onClick={this.onType}/>
                                    <label className="add-type-label">Home</label>
                                    <input className="radio-btn" type="radio" id="2" name="type" onClick={this.onType}/>
                                    <label className="add-type-label">Work</label>
                                </div>
                                <hr/>
                                <button className="popup-add-btn" onClick={() => this.checkOut(orderIds, total, true)}
                                        disabled={this.state.selectAdd}>
                                    Add address
                                </button>
                            </div>
                        </div>
                    </Popup>
                </div>
            </div>
        )
    }
}
Orders.propTypes = {
    menus: PropTypes.object,
    order: PropTypes.object
}
export default Orders;