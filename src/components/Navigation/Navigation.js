import React from 'react';
import './Navigation.css';

const Navigation = ({onRouteChange , routes, userName}) => {
    return (
        <div>
            <header className="black-80 tc pv4 avenir">
                <p href="" className="bg-black-80 ba b--black dib pa3 w2 h2 br-100">
                    <svg className="white" data-icon="info" viewBox="0 0 32 32" style={{fill:'currentcolor'}}>
                        <title>Welcome to Spicy Corner</title>
                        <path d="M16 0 A16 16 0 0 1 16 32 A16 16 0 0 1 16 0 M19 15 L13 15 L13 26 L19 26 z M16 6 A3 3 0 0 0 16 12 A3 3 0 0 0 16 6"></path>
                    </svg>
                </p>
                <h1 className="mt2 mb0 baskerville i fw1 f1 pointer" onClick={()=>onRouteChange('')}>Spicy Corner</h1>
                <h2 className="mt2 mb0 f6 fw4 ttu tracked">We deliver Happiness...</h2>
                <nav className=" tr mw7 center mt4 nav-bg">
                    <p className=" pointer f6 f5-l link black-80 hover-bg-white dib pa3 ph4-l user-name">{routes === 'menu' ? 'Hello, '+userName  : null}</p>
                </nav>
            </header>
        </div>
    );
}

export default Navigation;