import React from 'react';
import SideNav, {Nav, NavIcon, NavText} from 'react-sidenav';
import SvgIcon from 'react-icons-kit';

import {ic_aspect_ratio} from 'react-icons-kit/md/ic_aspect_ratio';
import {ic_business} from 'react-icons-kit/md/ic_business';
import {ic_menu} from 'react-icons-kit/md/ic_menu';
import {ic_restaurant_menu} from 'react-icons-kit/md/ic_restaurant_menu';
import './SideNavigation.css';


//specify the base color/background of the parent container if needed
const SideNavigation = () => {
    return (
        <div className="container-sidenav">
            <SideNav highlightColor='#000' highlightBgColor='#fff' defaultSelected='sales'>
                <Nav id='dashboard'>
                    <NavIcon><SvgIcon size={20} icon={ic_aspect_ratio}/></NavIcon>
                    <NavText> Dashboard </NavText>
                </Nav>
                <Nav id='Veg'>
                    <NavIcon><SvgIcon size={20} icon={ic_restaurant_menu}/></NavIcon>
                    <NavText> Veg </NavText>
                </Nav>
                <Nav id='Non-Veg'>
                    <NavIcon><SvgIcon size={20} icon={ic_restaurant_menu}/></NavIcon>
                    <NavText> Non-Veg </NavText>
                </Nav>
                <Nav id='Starter'>
                    <NavIcon><SvgIcon size={20} icon={ic_restaurant_menu}/></NavIcon>
                    <NavText> Starter </NavText>
                </Nav>
                <Nav id='Main Course'>
                    <NavIcon><SvgIcon size={20} icon={ic_restaurant_menu}/></NavIcon>
                    <NavText> Main Course </NavText>
                </Nav>
                <Nav id='Breads'>
                    <NavIcon><SvgIcon size={20} icon={ic_restaurant_menu}/></NavIcon>
                    <NavText> Breads </NavText>
                </Nav>
                <Nav id='Rice & Biryani'>
                    <NavIcon><SvgIcon size={20} icon={ic_restaurant_menu}/></NavIcon>
                    <NavText> Rice & Biryani </NavText>
                </Nav>
                <Nav id='Fried Rice & Noodles'>
                    <NavIcon><SvgIcon size={20} icon={ic_restaurant_menu}/></NavIcon>
                    <NavText> Fried Rice & Noodles </NavText>
                </Nav>
                <Nav id='Rolls'>
                    <NavIcon><SvgIcon size={20} icon={ic_restaurant_menu}/></NavIcon>
                    <NavText> Rolls </NavText>
                </Nav>
                <Nav id='Snacks'>
                    <NavIcon><SvgIcon size={20} icon={ic_restaurant_menu}/></NavIcon>
                    <NavText> Snacks </NavText>
                </Nav>
                <Nav id='Dessert & Beverages'>
                    <NavIcon><SvgIcon size={20} icon={ic_restaurant_menu}/></NavIcon>
                    <NavText> Dessert & Beverages </NavText>
                </Nav>
            </SideNav>
        </div>
    )
}

export default SideNavigation;