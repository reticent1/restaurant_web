import React from 'react';
import './SignUpModal.css';

class SignUpModal extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            showModal: true,
            emailVal: '',
            passwordVal: '',
            nameVal: '',
            isLogin: false,
            hideContent: true,
            emailvalid: true,
            emailblank: true,
            invalidCred: true,
            passblank: true,
            passvalid: true,
            invalidPass: true,
            nameblank: true,
            namevalid: true,
            userexist:true
        }
    }

    handelModal = ()=> {
        this.setState({showModal: !this.state.showModal})
    }

    handelContent = ()=> {
        this.setState({hideContent: false})
    }

    handelSignin = ()=> {
        this.setState({hideContent: true})
    }

    emailField = (event)=> {
        const mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        this.setState({emailVal: event.target.value})
        if (event.target.value.match(mailformat)) {
            this.setState({emailvalid: true})
        } else if (event.target.value.trim() === "") {
            this.setState({emailblank: false, emailvalid: true})
        } else {
            this.setState({emailvalid: false, emailblank: true})
        }
    }

    passwordField = (event)=> {
        if (event.target.value.trim() === "") {
            this.setState({passblank: false, passvalid: true})
        }
        if (event.target.value.trim() !== "") {
            this.setState({passblank: true, passwordVal: event.target.value})
            if (event.target.value.length < 8) {
                this.setState({passvalid: false})
            } else {
                this.setState({passvalid: true})
            }
        }
    }

    nameField = (event)=> {
        if (event.target.value.trim() === "") {
            this.setState({nameblank: false, namevalid: true})
        }
        if (event.target.value.trim() !== "") {
            this.setState({nameblank:true})
            if (event.target.value.length < 3) {
                this.setState({namevalid: false})
            }
            if (event.target.value.length >= 3) {
                this.setState({namevalid: true, nameVal: event.target.value})
            }
        }
    }

    onLogin = ()=> {
        const {emailVal, passwordVal}= this.state;
        const mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (emailVal.match(mailformat)) {
            fetch('http://localhost:3000/signin', {
                method: 'post',
                headers: {'content-type': 'application/json'},
                body: JSON.stringify({
                    email: emailVal,
                    password: passwordVal
                })
            })
                .then(response => response.json())
                .then(data => {
                    if (data === 'Invalid credential' || data === 'unable to fetch') {
                        this.setState({invalidCred: false, invalidPass: true})
                        this.props.onRouteChange('')
                    } else if (data === 'password not matched') {
                        this.setState({invalidPass: false, invalidCred: true})
                        this.props.onRouteChange('')
                    } else {
                        this.setState({isLogin: true})
                        this.props.isLoggedIn(this.state.isLogin, emailVal)
                        this.props.onRouteChange('menu')
                        console.log(this.state.isLogin)
                    }
                })
        }
        if (emailVal.trim() === "") {
            this.setState({emailblank: false, emailvalid: true})
        }
        if (passwordVal.trim() === "") {
            this.setState({passblank: false, passvalid: true})
        }
    }

    onRegister = () => {
        const {nameVal, emailVal, passwordVal}=this.state;
        const mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (emailVal.match(mailformat) && nameVal !== null && passwordVal !== null) {
            fetch('http://localhost:3000/signup', {
                method: 'post',
                headers: {'content-type': 'application/json'},
                body: JSON.stringify({
                    name: nameVal,
                    email: emailVal,
                    password: passwordVal
                })
            })
                .then(response => response.json())
                .then(data => {
                    if (data === 'user already register') {
                        this.setState({userexist:false})
                        this.props.onRouteChange('')
                    } else if(data === 'name and password'){
                        this.props.onRouteChange('')
                    }
                    else {
                        this.props.loadUser(data);
                        this.setState({isLogin: true})
                        this.props.isLoggedIn(this.state.isLogin, emailVal)
                        this.props.onRouteChange('menu')
                    }
                })
                .catch(error => {
                    console.log(error)
                })
        }
        if (emailVal.trim() === "") {
            this.setState({emailblank: false, emailvalid: true})
        }
        if (passwordVal.trim() === "") {
            this.setState({passblank: false, passvalid: true})
            this.props.onRouteChange('')
        }
        if (nameVal.trim() === "") {
            this.setState({nameblank: false, namevalid: true})
        }
    }

    render() {
        return (
            <div>
                <div className="centered-signup">
                    <button onClick={this.handelModal} className="btn-signup">Sign Up</button>
                </div>
                <div hidden={this.state.showModal} className="modal">
                    <div className="modal-content">
                        <span className="close" onClick={this.handelModal}>&times;</span>
                        <div className="modal-btn-grp">
                            <button className={`modal-btns ${this.state.hideContent ? 'active' : null}`}
                                    onClick={this.handelSignin}>Sing in
                            </button>
                            <button className={`modal-btns ${this.state.hideContent ? null : 'active'}`}
                                    onClick={this.handelContent}>Sing up
                            </button>
                        </div>
                        <div className="social-modal-content">
                            <button className="facebook-login-button">
                                <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25"
                                     viewBox="0 0 266.893 266.895">
                                    <path
                                        d="M252.164 266.895c8.134 0 14.729-6.596 14.729-14.73V14.73c0-8.137-6.596-14.73-14.729-14.73H14.73C6.593 0 0 6.594 0 14.73v237.434c0 8.135 6.593 14.73 14.73 14.73h237.434z"
                                        fill="#fff"></path>
                                    <path
                                        d="M184.152 266.895V163.539h34.692l5.194-40.28h-39.887V97.542c0-11.662 3.238-19.609 19.962-19.609l21.329-.01V41.897c-3.689-.49-16.351-1.587-31.08-1.587-30.753 0-51.807 18.771-51.807 53.244v29.705h-34.781v40.28h34.781v103.355h41.597z"
                                        fill="#3b5998"></path>
                                </svg>
                                <span>Continue with Facebook</span>
                            </button>
                            <button className="google-login-button">
                                <svg width="25" height="25" viewBox="0 0 25 25">
                                    <g fill="none" fillRule="evenodd">
                                        <path
                                            d="M20.66 12.693c0-.603-.054-1.182-.155-1.738H12.5v3.287h4.575a3.91 3.91 0 0 1-1.697 2.566v2.133h2.747c1.608-1.48 2.535-3.65 2.535-6.24z"
                                            fill="#4285F4"></path>
                                        <path
                                            d="M12.5 21c2.295 0 4.22-.76 5.625-2.06l-2.747-2.132c-.76.51-1.734.81-2.878.81-2.214 0-4.088-1.494-4.756-3.503h-2.84v2.202A8.498 8.498 0 0 0 12.5 21z"
                                            fill="#34A853"></path>
                                        <path
                                            d="M7.744 14.115c-.17-.51-.267-1.055-.267-1.615s.097-1.105.267-1.615V8.683h-2.84A8.488 8.488 0 0 0 4 12.5c0 1.372.328 2.67.904 3.817l2.84-2.202z"
                                            fill="#FBBC05"></path>
                                        <path
                                            d="M12.5 7.38c1.248 0 2.368.43 3.25 1.272l2.437-2.438C16.715 4.842 14.79 4 12.5 4a8.497 8.497 0 0 0-7.596 4.683l2.84 2.202c.668-2.01 2.542-3.504 4.756-3.504z"
                                            fill="#EA4335"></path>
                                    </g>
                                </svg>
                                <span>Continue with Google</span>
                            </button>
                            <p className="social-method-seprator">or</p>
                            <div className="login-modal-form">
                                <div className="form-group" hidden={this.state.hideContent}>
                                    <label htmlFor="email">Name</label>
                                    <input type="text"
                                           className={`form-control ${this.state.nameblank ? null : 'error-element'}`}
                                           id="name" name="name"
                                           placeholder="NickName"
                                           onChange={this.nameField}
                                    />
                                    <span hidden={this.state.nameblank}
                                          className="error-span">Please enter your name</span>
                                    <span hidden={this.state.namevalid} className="error-span">Name must be greater than 3 characters</span>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="email">Email</label>
                                    <input type="email"
                                           className={`form-control ${this.state.emailblank ? null : 'error-element'}`}
                                           id="email" name="email"
                                           placeholder="Email"
                                           onChange={this.emailField}
                                           autoFocus="autoFocus"
                                    />
                                    <span hidden={this.state.emailvalid} className="error-span">Invalid Email id</span>
                                    <span hidden={this.state.emailblank} className="error-span">Please enter your email id</span>
                                </div>
                                <div className="form-group">
                                    <label htmlFor="password">Password</label>
                                    <input type="password"
                                           className={`RML-form-control ${this.state.passblank ? null : 'error-element'}`}
                                           id="password" name="password"
                                           placeholder="Password"
                                           onChange={this.passwordField}
                                    />
                                    <span hidden={this.state.passblank} className="error-span">Please enter your password</span>
                                    <span hidden={this.state.passvalid} className="error-span">password must be 8 characters long</span>
                                </div>
                            </div>
                            <div hidden={!this.state.hideContent}>
                                <button className="login-btn" onClick={this.onLogin}>Sign in</button>
                            </div>
                            <div hidden={this.state.hideContent}>
                                <button className="login-btn" onClick={this.onRegister}>Sign up</button>
                            </div>
                            <span className="error-span" hidden={this.state.invalidCred}> ! Invalid credentials</span>
                            <span className="error-span" hidden={this.state.invalidPass}> ! Incorrect Password</span>
                            <span className="error-span" hidden={this.state.userexist}> User already registered</span>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default SignUpModal;