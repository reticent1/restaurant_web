import React from 'react';
import './Footer.css';
import FB from './icons/hdfb.png';
import TW from './icons/hdtwitter.png';
import GP from './icons/hdGp.png';
import ISG from './icons/hdinsta.png';
import {FaHeart} from 'react-icons/fa';
import Home from './icons/home2.png'
import Mob from './icons/mob1.png';
import Mail from './icons/mail1.png';
import  WOW from 'wowjs'
import '../css/animate.css';
class Footer extends React.Component {
    componentDidMount() {
        const wow = new WOW.WOW();
        wow.init()
    }

    render() {
        return (
            <div className="footer-container-all">
                <div className="footer-container">
                    <div className="footer-col1">
                        <h1>About Website</h1>
                        <p>Made With</p><p className="wow pulse" data-wow-iteration="infinite">
                            <FaHeart style={{'color': 'red', 'fontSize': '20px'}}/>
                        </p>
                        <p>Front end: ReactJs,html,css</p>
                        <p>Back end: ExpressJs</p>
                        <p>Database: Postgresql</p>
                    </div>

                    <div className="footer-col2">
                        <h1>Stay in Touched</h1>
                        <div className="row2">
                            <img className="wow bounceIn" data-wow-delay="500ms"
                                 src={FB}
                                 alt=""
                            />
                            <label>
                                Like " The Webdeveloper's " page on Facebook
                            </label>
                        </div>
                        <div className="row2">
                            <img className="wow bounceIn" data-wow-delay="550ms"
                                 src={TW}
                                 alt=""
                            />
                            <label>
                                Follow " The Webdeveloper's " on Twitter
                            </label>
                        </div>
                        <div className="row2">
                            <img className="wow bounceIn" data-wow-delay="600ms"
                                 src={GP}
                                 alt=""
                            />
                            <label>
                                Follow us on Google Plus
                            </label>
                        </div>
                        <div className="row2">
                            <img className="wow bounceIn" data-wow-delay="650ms"
                                 src={ISG}
                                 alt=""
                            />
                            <label>
                                Follow us on Instagram
                            </label>
                        </div>
                    </div>

                    <div className="footer-col3">
                        <h1>Contact Information</h1>
                        <div className="row3">
                            <img className="wow bounceIn" data-wow-delay="500ms"
                                 src={Home}
                            />
                            <p>
                                Kamlesh Devchand Mohane,<br/>
                                Web developer,
                                Karvenagar kothrud Pune
                                411052
                            </p>
                        </div>
                        <div className="row3">
                            <img className="wow bounceIn" data-wow-delay="550ms"
                                 src={Mob}
                            />
                            <p>
                                +91 8830374665
                            </p>
                        </div>
                        <div className="row3">
                            <img className="wow bounceIn" data-wow-delay="600ms"
                                 src={Mail}
                            />
                            <p>
                                kamleshmohane78@gmail.com
                            </p>
                        </div>
                    </div>
                </div>
                <div className="footer-bottom">
                    <div className="copyright">
                        <label>@All right reserved 2018</label>
                    </div>
                    <div className="imp-company-link">
                        <label>company | privacy Policy | Terms and Condition</label>
                    </div>
                </div>
            </div>
        )
    }
}

export default Footer;