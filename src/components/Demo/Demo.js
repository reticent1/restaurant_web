import React from 'react';
import './demo.css';
import restaurant from './images/restaurant1.jpg'
import ptikka from './images/paneertikka.jpg'
import gobimanchu from './images/gobimanchurian.jpg'
import pavbhaji from './images/pavbhaji.jpg'
import mushroom from './images/mushroom.jpg';
import chickenb from './images/chickenbiryani.jpg'
import chickent from './images/nonvegthali.jpg'
import schezwan from './images/schezwan.jpg';
import chef from './images/chef1 .jpg'
import punjabit from './images/punjabithali.jpg'
import  WOW from 'wowjs'
import '../css/animate.css';

class Demo extends React.Component {
    componentDidMount(){
        const wow =new WOW.WOW();
        wow.init()
    }
    render() {
        return (
            <div>
                <div className="content-container">
                    <div>
                        <div>
                            <h1 className="wow restaurant-name lightSpeedIn">
                                Our Restaurant
                            </h1>
                            <div className="restaurant-discription">
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                    Accusamus adipisci alias aliquid animi cupiditate dicta dolorum,
                                    error facilis hic illum inventore magnam nulla pariatur porro quisquam quod rem
                                    sapiente!
                                    Perferendis!
                                </p>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                    Aliquid animi autem error ex facilis illum ipsa itaque necessitatibus non obcaecati
                                    odit
                                    praesentium quae quisquam,
                                    quos recusandae repellendus unde veniam voluptate!
                                </p>
                            </div>
                            <div className="restaurant-main-img">
                                <img src={restaurant} className="rest-image" alt="400x300"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="content-container">
                    <div>
                        <div>
                            <h1 className="wow restaurant-name lightSpeedIn">
                                Our Chef's
                            </h1>
                            <div className="restaurant-discription restaurant-sub-discription">
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                    Accusamus adipisci alias aliquid animi cupiditate dicta dolorum,
                                    error facilis hic illum inventore magnam nulla pariatur porro quisquam quod rem
                                    sapiente!
                                    Perferendis!
                                </p>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                    Aliquid animi autem error ex facilis illum ipsa itaque necessitatibus non obcaecati
                                    odit
                                    praesentium quae quisquam,
                                    quos recusandae repellendus unde veniam voluptate!
                                </p>
                            </div>
                            <div className="restaurant-img restaurant-sub-image">
                                <img src={chef} className="rest-image" alt="400x300"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="content-container">
                    <div>
                        <div>
                            <div className="restaurant-discription">
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                    Accusamus adipisci alias aliquid animi cupiditate dicta dolorum,
                                    error facilis hic illum inventore magnam nula pariatur porro quisquam quod rem
                                    sapiente!
                                    Perferendis!
                                </p>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                    Aliquid animi autem error ex facilis illum ipsa itaque necessitatibus non obcaecati
                                    odit
                                    praesentium quae quisquam,
                                    quos recusandae repellendus unde veniam voluptate!
                                </p>
                            </div>
                            <div className="restaurant-img">
                                <img src={chef} className="rest-image" alt="400x300"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="content-container">
                    <div>
                        <div>
                            <h1 className="wow restaurant-name lightSpeedIn">
                                Our Speciality
                            </h1>
                            <div className="special-items">
                                <img className="special-img wow fadeInUpBig" src={ptikka} alt="200x300" data-wow-delay="0ms"/>
                                <img className="special-img wow fadeInUpBig" data-wow-delay="50ms"  src={gobimanchu} alt="200x300"/>
                                <img className="special-img wow fadeInUpBig" data-wow-delay="100ms" src={pavbhaji} alt="200x300"/>
                                <img className="special-img wow fadeInUpBig" data-wow-delay="150ms" src={mushroom} alt="200x300"/>
                                <img className="special-img wow fadeInUpBig" src={chickenb} alt="200x300" data-wow-delay="200ms"/>
                                <img className="special-img wow fadeInUpBig" src={chickent} alt="200x300" data-wow-delay="250ms"/>
                                <img className="special-img wow fadeInUpBig" src={schezwan} alt="200x300" data-wow-delay="300ms"/>
                                <img className="special-img wow fadeInUpBig" src={punjabit} alt="200x300" data-wow-delay="350ms"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="content-container">
                    <div>
                        <div>
                            <h1 className="wow restaurant-name lightSpeedIn">
                                Visit Us
                            </h1>
                            <div className="visit-us">
                                <iframe
                                    title="google-map"
                                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5351.1712706784465!2d73.82025160616958!3d18.49047541767304!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bc2bfdb81e11525%3A0x5904c195b8487d4a!2sDatta+Mandir!5e0!3m2!1sen!2sin!4v1538744914660"
                                    className="google-map-frame"
                                    frameBorder="0"
                                    style={{'border': '0'}}
                                    allowFullScreen>
                                </iframe>
                            </div>
                            <div className="visit-us-order">
                                <label className="home-add-label">
                                    Visit us at:
                                </label>
                                <p className="home-add">Karvenagar kothrud near datta mandir</p>
                                <p className="home-add">Pincode: 411052</p>
                                <p className="home-add">Pune, Maharashtra</p>
                                <p className="home-add">Mobile no.:+91 8830374665</p>
                                <button className="home-order">Order online</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Demo;
