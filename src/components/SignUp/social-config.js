const facebook = {
    appId: 247736439273349,
    cookie: true,
    xfbml: true,
    version: 'v3.1',
    scope: 'email'
}

const google = {
    client_id: '740878409484-232hrdeqhli3bhndkfkupebtp79d6ill.apps.googleusercontent.com',
    scope: "https://www.googleapis.com/auth/plus.login"
}

export const googleConfig = google;

export const facebookConfig = facebook;


