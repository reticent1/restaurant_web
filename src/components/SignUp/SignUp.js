import React from 'react';
import ReactModalLogin from 'react-modal-login';
import './SignUp.css';
import {facebookConfig, googleConfig} from './social-config';

// const SignUp = () => {
//     return (
//         <Popup trigger={<div className="centered-signup">
//             <button className="btn-signup">Sign Up</button>
//         </div>}
//                modal
//                closeOnDocumentClick
//         >
//             {close => (
//                 <div>
//                     <a className="close" onClick={close}>
//                         &times;
//                     </a>
//                     <div className="header">
//                         Sign Up to Spicy Corner
//                     </div>
//                     <hr/>
//                     <div className="content-signup">
//                         <label className="label-item">Full Name</label>
//                         <input type="text" name="fname" placeholder="Name" className="input-item"/>
//                         <label className="label-item">Email</label>
//                         <input type="email" name="email" placeholder="Email Address" className="input-item"/>
//                         <label className="label-item">Password</label>
//                         <input type="password" name="password" placeholder="Password" className="input-item"/>
//                     </div>
//                     <div>
//                         <button className="btn-register">Register</button>
//                     </div>
//                 </div>
//             )}
//         </Popup>
//     );
// }
/*class SignUp extends React.Component {

 constructor(props) {
 super(props);

 this.state = {
 showModal: false,
 loading: false,
 error: null
 };

 }

 openModal() {
 this.setState({
 showModal: true,
 });
 }

 closeModal() {
 this.setState({
 showModal: false,
 error: null
 });
 }

 onLoginSuccess(method, response) {
 console.log('logged successfully with ' + method);
 }

 onLoginFail(method, response) {
 console.log('logging failed with ' + method);
 this.setState({
 error: response
 })
 }

 startLoading() {
 this.setState({
 loading: true
 })
 }

 finishLoading() {
 this.setState({
 loading: false
 })
 }

 afterTabsChange() {
 this.setState({
 error: null
 });
 }

 onSelectTab(tab) {

 this.setState({
 newTab: tab
 }, () => {
 this.setState({
 newTab: null
 })
 })
 }

 render() {

 return (
 <div>
 <div className="centered-signup">
 <button className="btn-signup"
 onClick={() => this.openModal()}
 >
 Sign Up
 </button>
 </div>
 <ReactModalLogin
 newTab={this.state.newTab}
 visible={this.state.showModal}
 onCloseModal={this.closeModal.bind(this)}
 loading={this.state.loading}
 error={this.state.error}
 tabs={{
 afterChange: this.afterTabsChange.bind(this)
 }}
 loginError={{
 label: "Couldn't sign in, please try again."
 }}
 registerError={{
 label: "Couldn't sign up, please try again."
 }}
 startLoading={this.startLoading.bind(this)}
 finishLoading={this.finishLoading.bind(this)}
 providers={{
 facebook: {
 config: facebookConfig,
 onLoginSuccess: this.onLoginSuccess.bind(this),
 onLoginFail: this.onLoginFail.bind(this),
 label: "Continue with Facebook"
 },
 google: {
 config: googleConfig,
 onLoginSuccess: this.onLoginSuccess.bind(this),
 onLoginFail: this.onLoginFail.bind(this),
 label: "Continue with Google"
 }
 }}
 />
 </div>
 )
 }
 }

 export default SignUp;*/
export default class SignUp extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            showModal: false,
            loggedIn: null,
            loading: false,
            error: null,
            initialTab: null,
            recoverPasswordSuccess: null,
            isLogin: false,
            emailId: ''
        };

    }

    onLogin() {
        console.log('__onLogin__');
        console.log('email: ' + document.querySelector('#email').value);
        console.log('password: ' + document.querySelector('#password').value);

        const email = document.querySelector('#email').value;
        const password = document.querySelector('#password').value;

        if (email.length < 1 && password.length < 5) {

        } else {
            fetch('http://localhost:3000/signin', {
                method: 'post',
                headers: {'content-type': 'application/json'},
                body: JSON.stringify({
                    email: email,
                    password: password
                })
            })
                .then(response => response.json())
                .then(data => {
                    if (data === 'Invalid credential' || data === 'unable to fetch') {
                        this.props.onRouteChange('')
                    } else {
                        this.setState({isLogin: true, emailId: email})
                        this.props.isLoggedIn(this.state.isLogin, this.state.emailId)
                        this.props.onRouteChange('menu')
                        console.log(this.state.emailId)
                        console.log(this.state.isLogin)
                    }
                })
        }
    }

    onRegister() {
        console.log('__onRegister__');
        console.log('login: ' + document.querySelector('#login').value);
        console.log('email: ' + document.querySelector('#email').value);
        console.log('password: ' + document.querySelector('#password').value);

        const login = document.querySelector('#login').value;
        const email = document.querySelector('#email').value;
        const password = document.querySelector('#password').value;

        fetch('http://localhost:3000/signup', {
            method: 'post',
            headers: {'content-type': 'application/json'},
            body: JSON.stringify({
                name: login,
                email: email,
                password: password
            })
        })
            .then(response => response.json())
            .then(data => {
                if (data === 'user already register') {
                    console.log('user already exist')
                    this.props.onRouteChange('')
                }
                else {
                    this.props.loadUser(data);
                    this.setState({isLogin: true, emailId: email})
                    this.props.isLoggedIn(this.state.isLogin, this.state.emailId)
                    this.props.onRouteChange('menu')
                }
            })
            .catch(error => {
                console.log(error)
            })
    }

    onRecoverPassword() {
        console.log('__onFotgottenPassword__');
        console.log('email: ' + document.querySelector('#email').value);

        const email = document.querySelector('#email').value;


        if (!email) {
            this.setState({
                error: true,
                recoverPasswordSuccess: false
            })
        } else {
            this.setState({
                error: null,
                recoverPasswordSuccess: true
            });
        }
    }

    openModal(initialTab) {
        this.setState({
            initialTab: initialTab
        }, () => {
            this.setState({
                showModal: true,
            })
        });
    }

    onLoginSuccess(method, response) {

        this.closeModal();
        this.setState({
            loggedIn: method,
            loading: false
        })
    }

    onLoginFail(method, response) {
        console.log('logging failed with ' + method);
        this.setState({
            error: response
        })
    }

    startLoading() {
        this.setState({
            loading: true
        })
    }

    finishLoading() {
        this.setState({
            loading: false
        })
    }

    afterTabsChange() {
        this.setState({
            error: null,
            recoverPasswordSuccess: false,
        });
    }

    closeModal() {
        this.setState({
            showModal: false,
            error: null
        });
    }

    render() {

        const loggedIn = this.state.loggedIn
            ? <div>
            <p>You are signed in with: {this.state.loggedIn}</p>
        </div>
            : <div>
            <p>You are signed out</p>
        </div>;

        const isLoading = this.state.loading;

        return (
            <div>
                <div className="centered-signup">
                    <button
                        className="btn-signup"
                        onClick={() => this.openModal('login')}
                    >
                        Sign up
                    </button>
                </div>
                <ReactModalLogin
                    visible={this.state.showModal}
                    onCloseModal={this.closeModal.bind(this)}
                    loading={isLoading}
                    initialTab={this.state.initialTab}
                    error={this.state.error}
                    tabs={{
                        afterChange: this.afterTabsChange.bind(this)
                    }}
                    loginError={{
                        label: "Couldn't sign in, please try again."
                    }}
                    registerError={{
                        label: "Couldn't sign up, please try again."
                    }}
                    startLoading={this.startLoading.bind(this)}
                    finishLoading={this.finishLoading.bind(this)}
                    form={{
                        onLogin: this.onLogin.bind(this),
                        onRegister: this.onRegister.bind(this),
                        onRecoverPassword: this.onRecoverPassword.bind(this),
                        onLoginFail: this.onLoginFail.bind(this),

                        recoverPasswordSuccessLabel: this.state.recoverPasswordSuccess
                            ? {
                            label: "New password has been sent to your mailbox!"
                        }
                            : null,
                        recoverPasswordAnchor: {
                            label: "Forgot your password?"
                        },
                        loginBtn: {
                            label: "Sign in"
                        },
                        registerBtn: {
                            label: "Sign up"
                        },
                        recoverPasswordBtn: {
                            label: "Send new password"
                        },
                        loginInputs: [
                            {
                                containerClass: 'RML-form-group tl',
                                label: 'Email',
                                type: 'email',
                                inputClass: 'RML-form-control',
                                id: 'email',
                                name: 'email',
                                placeholder: 'Email'
                            },
                            {
                                containerClass: 'RML-form-group tl',
                                label: 'Password',
                                type: 'password',
                                inputClass: 'RML-form-control',
                                id: 'password',
                                name: 'password',
                                placeholder: 'Password',
                            }
                        ],
                        registerInputs: [
                            {
                                containerClass: 'RML-form-group tl',
                                label: 'Nickname',
                                type: 'text',
                                inputClass: 'RML-form-control',
                                id: 'login',
                                name: 'login',
                                placeholder: 'Nickname',
                            },
                            {
                                containerClass: 'RML-form-group tl',
                                label: 'Email',
                                type: 'email',
                                inputClass: 'RML-form-control',
                                id: 'email',
                                name: 'email',
                                placeholder: 'Email',
                            },
                            {
                                containerClass: 'RML-form-group tl',
                                label: 'Password',
                                type: 'password',
                                inputClass: 'RML-form-control',
                                id: 'password',
                                name: 'password',
                                placeholder: 'Password',
                            }
                        ],
                        recoverPasswordInputs: [
                            {
                                containerClass: 'RML-form-group',
                                label: 'Email',
                                type: 'email',
                                inputClass: 'RML-form-control',
                                id: 'email',
                                name: 'email',
                                placeholder: 'Email',
                            },
                        ],
                    }}
                    separator={{
                        label: "or"
                    }}
                    providers={{
                        facebook: {
                            config: facebookConfig,
                            onLoginSuccess: this.onLoginSuccess.bind(this),
                            onLoginFail: this.onLoginFail.bind(this),
                            inactive: isLoading,
                            label: "Continue with Facebook"
                        },
                        google: {
                            config: googleConfig,
                            onLoginSuccess: this.onLoginSuccess.bind(this),
                            onLoginFail: this.onLoginFail.bind(this),
                            inactive: isLoading,
                            label: "Continue with Google"
                        }
                    }}
                />
                {console.log(loggedIn)}
            </div>
        )
    }
}
