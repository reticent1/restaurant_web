import React, {Component} from 'react';
import './App.css';
import Navigation from './components/Navigation/Navigation';
import Home from './components/HomePage/home';
import SignUp from './components/SignUp/SignUp';
import Demo from './components/Demo/Demo';
import MenuCard from './components/MenuCard/MenuCard';
import Orders from './components/Orders/Orders'
import PaymentMode from './components/PaymentMode/PaymentMode'
import Footer from './components/Footer/Footer';
import SignUpModal from './components/SignUp Modal/SignUpModal';

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            route: '',
            user: {
                id: '',
                name: '',
                email: '',
                password: ''
            },
            menus: [],
            category: {},
            order: {},
            isLogin: false,
            emailId: '',
            userName: '',
            clsName : ''
        }
    }

    loadUser = (data) => {
        this.setState({
            user: {
                id: data.id,
                name: data.name,
                email: data.email,
                password: data.password
            }
        })
    };

    isLoggedIn = (isSignIn, email) => {
        this.setState({isLogin: isSignIn, emailId: email})
    };

    userName = (name)=> {
        this.setState({userName: name})
    };

    onRouteChange = (route) => {
        this.setState({route: route})
    };

    componentDidMount() {
        fetch('http://localhost:3000/menu/food_category', {
            method: 'get',
            headers: {'content-type': 'application/json'}
        })
            .then(response => response.json())
            .then(menudata => {
                this.setState({category: menudata});
                console.log(this.state.category)
            });

        fetch('http://localhost:3000/menu', {
            method: 'get',
            headers: {'content-type': 'application/json'}
        })
            .then(response => response.json())
            .then(menudata => {
                this.setState({menus: menudata})
            })
    }

    addToOrder = (key) => {
        let check = false;
        const order = {...this.state.order};
        order[key] = order[key] + 1 || 1;
        this.setState({order})
        var x=document.getElementById(key)
        x.style.backgroundColor="green";
        x.style.color="white";
    };

    removeOrder = (key) => {
        const order = {...this.state.order};
        delete order[key];
        this.setState({order})
        var x=document.getElementById(key)
        x.style.backgroundColor="white";
        x.style.color="black";
    };

    render() {
        return (
            <div className="App">
                <Navigation
                    onRouteChange={this.onRouteChange}
                    userName={this.state.userName}
                    routes={this.state.route}
                />
                {this.state.route === 'menu' ?
                    <div className="scroll-menu">
                        {this.state.category.map((types ) => {
                            return (<div>
                                <div className="menu-head">{types.menu_type}</div>
                                < div className="container-menu">
                                    {this.state.menus.map((d, key)=> {
                                        if (d.category === types.menu_type) {
                                            return (
                                                <div className="content">
                                                    <div className="items"> {d.item_name}</div>
                                                    <div className="prices"> {d.price} Rs.</div>
                                                    <button key={key} id={key}
                                                            onClick={()=>this.addToOrder(key)}
                                                            className="add-menu-btn"> add
                                                    </button>
                                                </div>
                                            )
                                        }
                                    })}
                                </div>
                            </div>)
                        })}
                        <Orders
                            menus={this.state.menus}
                            order={this.state.order}
                            login={this.state.isLogin}
                            email={this.state.emailId}
                            name={this.userName}
                            removeOrder={this.removeOrder}
                            onRouteChange={this.onRouteChange}
                        />
                    </div> :
                    <div>
                        {this.state.route === 'payment' ?
                            <PaymentMode/> :
                            <div>
                                <Home/>
                                {/*< SignUp*/}
                                    {/*loadUser={this.loadUser}*/}
                                    {/*isLoggedIn={this.isLoggedIn}*/}
                                    {/*routes={this.state.route}*/}
                                    {/*onRouteChange={this.onRouteChange}*/}
                                {/*/>*/}
                                <SignUpModal
                                    loadUser={this.loadUser}
                                    isLoggedIn={this.isLoggedIn}
                                    routes={this.state.route}
                                    onRouteChange={this.onRouteChange}
                                />
                                < Demo />
                                <Footer/>
                            </div>
                        }
                    </div>
                }

            </div>
        );
    }
}

export default App;
